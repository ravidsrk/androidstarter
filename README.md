# Starter Tools for Android

This is an Android sample app + tests that will be used to work on various project to increase the quality of the Android platform.

# Usage

This section describes how to build & test the project using those different testing technologies.

## Install the Android SDK through maven-android-sdk-deployer

As it takes time to get android jars in maven central, including android UI automator jars in maven central,
we recommend to use [maven-android-sdk-deployer](https://github.com/mosabua/maven-android-sdk-deployer) to obtain android artefacts.
This step can also be executed on a CI server.

```bash
#install Android SDK 17 local files to local maven repo  
git clone git@github.com:mosabua/maven-android-sdk-deployer.git
cd maven-android-sdk-deployer/
mvn install -P 4.2
#Add V4 support library (to use FEST Android)
cd extras/compatibility-v4/
mvn install
```

## Standard Android testing APIs and code coverage using emma

To build the sample project and run the sample app on a plugged rooted device / running emulator : 

```bash
# in parent folder
mvn clean install -P emma
mvn sonar:sonar -P emma
```

you will get tests results in : target/surefire-reports/.
you will get tests coverage in : target/emma/.

## Robolectric and code coverage using cobertura

```bash
# in parent folder
mvn clean cobertura:cobertura -P cobertura
mvn sonar:sonar -P cobertura
```

## Espresso

To build the sample project and run the sample app on a plugged device / running emulator : 

```bash
# in parent folder
mvn clean install -P espresso
```

## Spoon from Squareup

```bash
# in parent folder
mvn clean install -P spoon

#then browse to android-sample-tests/target/spoon-output/index.html
```

## Using Gradle

All gradle-related file are stored in folder `gradle`.

With Gradle 1.8+ and android gradle plugin 0.6.+ : 

### build the app under tests

```bash
# in parent folder
gradle clean assemble
```

### launch the app under tests

```bash
# in parent folder
gradle :android-sample:installDebug
```

### play standard android tests (without emma coverage): 

```bash
# in parent folder
gradle clean assembleDebug connectedInstrumentTest
#export to sonar
gradle :android-sample:sonarRunner
```

### play espresso tests (without emma coverage): 

```bash
# in parent folder
gradle clean assembleDebug :android-sample-espresso-tests:connectedInstrumentTest
```

### play robolectric tests : 

```bash
# in parent folder
gradle clean assembleDebug robolectric
#export to sonar
gradle :android-sample-robolectric-tests:sonarRunner
```